#include <stdlib.h>
#include <stdio.h>
#include "list.h"
#include <string.h>


// copied from test.c
void print (element_t ev) {
  char* e = (char *) ev;
  printf ("%s ", e);
}

void print_int (element_t ev) {
  int* e = ev;
  printf ("%d\n", *e);
}

/*
    Checks if string is a number. If it is, the
    corresponding value in this new list should be
    that number, otherwise it should be -1.
**/
void toNum(element_t* rv, element_t av){
    char* a = (char*) av;
    int **r = (int **) rv;

    char *endp;
    int value = strtol(a, &endp, 10);

    if (*r == NULL)
        *r = malloc(sizeof(int));

    if (*endp)
        **r = -1;
    else
        **r = value;

}

/*
    Make NULL if number != -1
*/
void toNull(element_t* rv, element_t av, element_t bv){
    char* a = (char*) av;
    int* b = (int *) bv;
    char** r = (char**) rv;

    if (*r == NULL)
        *r = malloc(sizeof(int));

    if (*b == -1)
        *r = a;
    else
        **r = 0;
}

/*
*   Checks if a number is positive
*/
int isPositive (element_t av) {
  int *a = av;
  return (*a != -1);
}

/*
*   Checks if a number is not NULL
*/
int isNotNull (element_t av) {
    char *a = av;
    return (*a != '\0');
}

/*
* Truncate strings, on a pairwise basis, taking the values in
* the number list to be the maximum length of the entry
* in the string list
*/
void shorten (element_t* rv, element_t av, element_t bv) {
    char* a = (char*) av;
    int *b = (int*) bv;
    char** r = (char**) rv;

    *r = strdup(a);
    int str_len = strlen(*r);
    if (str_len > *b)
       (*r)[*b] = 0;
}

/*
* Puts max number (between *bv and *av) in **rv
*/
void max (element_t* rv, element_t av, element_t bv) {
    int *a = av;
    int *b = bv;
    int **r = (int**) rv;

    if (*b > *a){
       *a = *b;
    }
}


int main(int argc, char *argv[]) {

    // STEP 1: read a list of strings from command line
    struct list* l1 = list_create();
    for (int i = 1; i < argc; ++i)
    {
        list_append(l1, argv[i]);
    }

    // STEP 2: make a list of numbers
    struct list* l2 = list_create();
    list_map1 (toNum, l2, l1);

    // STEP 3: make list of strings with a NULL value if a number
    struct list* l3 = list_create();
    list_map2 (toNull, l3, l1, l2);

    // STEP 4: Remove negative values from numbers list
    struct list* l4 = list_create();
    list_filter(isPositive, l4, l2);

    // STEP 5: Remove all NULL values from string list
    struct list* l5 = list_create();
    list_filter(isNotNull, l5, l3);

    // STEP 6: Truncate strings
    struct list* l6 = list_create();
    list_map2(shorten, l6, l5, l4);

    // STEP 8: Print strings separated by space
    list_foreach (print, l6);

    // STEP 9: Print max number in the list
    int max_val = 0, *max_p = &max_val;
    list_foldl (max, (element_t*) &max_p, l4);
    printf("%i\n", max_val);

    // // STEP 10: free
    list_destroy(l1);
    list_destroy(l2);
    list_destroy(l3);
    list_destroy(l4);
    list_destroy(l5);
    list_destroy(l6);
}
